
function grorpArrayElements(array = [], N) {

    if(!Array.isArray(array) || isNaN(N)) {
        console.error('Please enter valid parameters and try again')
        return;
    }

    const newArray = [];
    const arrayLen = array && array.length;
    const noOfEqualDivisions = Math.ceil(arrayLen/N);

    for (let i = 0; i <= N+1; i += noOfEqualDivisions) {

        // push new sets of array into newly defined array
        newArray.push(array.slice(i, noOfEqualDivisions + i));
    }

    console.log('New refactored Array is:', newArray)
}


const array = [1,2,3,4,5, 6];
// This are examples of array that will return a failure message
// grorpArrayElements('Hi', 'test')
// grorpArrayElements(array, 'test')
// grorpArrayElements('Hi', 6)

grorpArrayElements(array, 3);